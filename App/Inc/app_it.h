/*
 * app_it.h
 *
 *  Created on: Dec 1, 2019
 *      Author: Igor
 */

#ifndef APP_IT_H_
#define APP_IT_H_

void LedStripDMA_ISR(void);

#endif /* APP_IT_H_ */
