/*
 * debug_utils.h
 *
 *  Created on: Dec 5, 2019
 *      Author: Igor
 */

#ifndef INC_DEBUG_UTILS_H_
#define INC_DEBUG_UTILS_H_

#ifdef DEBUG
#	define ENABLE_DEBUG_PRINT (1)
#endif

void vDebugInit(void);
void vDebugLog(const char *format, ...);

#endif /* INC_DEBUG_UTILS_H_ */
