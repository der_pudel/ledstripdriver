/*
 * WS2812.h
 *
 *  Created on: Nov 24, 2019
 *      Author: Igor
 */

#ifndef SRC_WS2812_H_
#define SRC_WS2812_H_

#include <app.h>

#define WS2812_LED_COUNT (24)




void WS2812_Init ();
void WS2812_SetColor (int8_t nr, uint8_t r, uint8_t g, uint8_t b);
void WS2812_SetColorAll (uint8_t r, uint8_t g, uint8_t b);



void WS2812_SetBrightness(uint8_t b);


void WS2812_DMA_Next(void);
void WS2812_TransferStart(void);
void WS2812_TransferStop (void);

void WS2812_DMA_ISR(void);

#endif /* SRC_WS2812_H_ */
