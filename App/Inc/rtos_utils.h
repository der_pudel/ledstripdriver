/*
 * rtos.h
 *
 *  Created on: Dec 1, 2019
 *      Author: Igor
 */

#ifndef RTOS_UTILS_H_
#define RTOS_UTILS_H_

#include "FreeRTOS.h"
#include "task.h"
#include "event_groups.h"


#define MAIN_PERI_PRIO (tskIDLE_PRIORITY + 2)
#define MAIN_TASK_PRIO (tskIDLE_PRIORITY + 1)



#define EVENT_HW_BTN (1 << 0)

extern EventGroupHandle_t hwEvent;




#endif /* RTOS_UTILS_H_ */
