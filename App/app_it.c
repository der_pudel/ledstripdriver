/*
 * app_it.c
 *
 *  Created on: Dec 1, 2019
 *      Author: Igor
 */

#include <app.h>
#include <app_it.h>
#include <rtos_utils.h>
#include <WS2812.h>



void LedStripDMA_ISR(void) {
	WS2812_DMA_ISR();
}
