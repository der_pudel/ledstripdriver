/*
 * WS2812.cpp
 *
 *  Created on: Nov 24, 2019
 *      Author: Igor
 */
#include <WS2812.h>
#include "main.h"


// GRB color scheme
#define WS2812_R_INDEX (1)
#define WS2812_G_INDEX (0)
#define WS2812_B_INDEX (2)


// Timing
#define WS2812_FREQ (800000ul)
#define TIMx_FREQ   (48000000ul)
#define TIMx_ARR    ((TIMx_FREQ/WS2812_FREQ)-1)

#define WS2812_T0H (18)
#define WS2812_T1H (36)


#define WS2812_DATA_SIZE (WS2812_LED_COUNT * 3)
static uint8_t led_data[WS2812_DATA_SIZE];



#define WS2812_BRIGHTNESS_DEFAULT (50)
#define WS2812_BRIGHTNESS_MAX    (100)

static uint8_t brightness = WS2812_BRIGHTNESS_DEFAULT;


#define TIMx TIM15
#define DMAx DMA1

void WS2812_Init () {

	LL_TIM_DisableARRPreload(TIMx);
	LL_TIM_SetAutoReload(TIMx, TIMx_ARR);
	LL_TIM_OC_SetCompareCH1(TIMx, 0);
	LL_TIM_CC_EnableChannel(TIMx, LL_TIM_CHANNEL_CH1);
	LL_TIM_EnableAllOutputs(TIMx);
	LL_TIM_EnableCounter(TIMx);

	WS2812_SetColorAll(0, 0, 0);

}

void WS2812_SetBrightness(uint8_t b) {

	if (b > WS2812_BRIGHTNESS_MAX) b = WS2812_BRIGHTNESS_MAX;
	brightness = b;
}

static inline uint8_t WS2812_BrightnessAdjust(uint8_t val) {
	uint8_t temp = (uint8_t) ( (((uint16_t)val) * brightness) / WS2812_BRIGHTNESS_MAX);
	if (temp == 0 && val !=0 ) temp = 1;
	return temp;
}

void WS2812_SetColor (int8_t nr, uint8_t r, uint8_t g, uint8_t b) {

	uint16_t offset = nr * 3;

	led_data[offset + WS2812_R_INDEX] =  WS2812_BrightnessAdjust(r);
	led_data[offset + WS2812_G_INDEX] =  WS2812_BrightnessAdjust(g);
	led_data[offset + WS2812_B_INDEX] =  WS2812_BrightnessAdjust(b);

}

void WS2812_SetColorAll (uint8_t r, uint8_t g, uint8_t b) {

	for (uint8_t i = 0; i < WS2812_LED_COUNT; i++) {
		WS2812_SetColor(i, r, g, b);
	}
}











// DMA stuff
#define WS2812_DMA_BUFFERS 	(2)
#define WS2812_DMA_BUFFER_SIZE (3 * 8)  // 8 bit * 3 colors
#define WS2812_DMA_TRANSFER_SIZE (WS2812_DMA_BUFFERS * WS2812_DMA_BUFFER_SIZE)

static uint16_t dma_buff[WS2812_DMA_BUFFERS][WS2812_DMA_BUFFER_SIZE];

static uint8_t dmaBuffIndex;
static uint8_t currentLed;


static void WS2812_DMA_PrepareNext(void);
static void WS2812_DMA_NullufyNext(void);

void WS2812_DMA_InitBuffer(void) {
	dmaBuffIndex = 0xFF;
	currentLed = 0;
	WS2812_DMA_Next();
	WS2812_DMA_Next();
}

void WS2812_DMA_ISR(void) {

	if (LL_DMA_IsActiveFlag_HT5(DMAx) || LL_DMA_IsActiveFlag_TC5(DMAx) ) {
		WS2812_DMA_Next();

		LL_DMA_ClearFlag_HT5(DMAx);
		LL_DMA_ClearFlag_TC5(DMAx) ;
	}
}

void WS2812_DMA_Next(void) {

	if (currentLed < WS2812_LED_COUNT) {
		WS2812_DMA_PrepareNext();
	} else if (currentLed == WS2812_LED_COUNT) {
		WS2812_DMA_NullufyNext();
	} else {
		WS2812_DMA_NullufyNext();
		WS2812_TransferStop();
	}
}

static inline void WS2812_DMA_PrepareByte(uint8_t byteNr, uint8_t color) {
	dma_buff[dmaBuffIndex][0 + byteNr*8] = (color & 0x80) ? WS2812_T1H : WS2812_T0H;
	dma_buff[dmaBuffIndex][1 + byteNr*8] = (color & 0x40) ? WS2812_T1H : WS2812_T0H;
	dma_buff[dmaBuffIndex][2 + byteNr*8] = (color & 0x20) ? WS2812_T1H : WS2812_T0H;
	dma_buff[dmaBuffIndex][3 + byteNr*8] = (color & 0x10) ? WS2812_T1H : WS2812_T0H;
	dma_buff[dmaBuffIndex][4 + byteNr*8] = (color & 0x08) ? WS2812_T1H : WS2812_T0H;
	dma_buff[dmaBuffIndex][5 + byteNr*8] = (color & 0x04) ? WS2812_T1H : WS2812_T0H;
	dma_buff[dmaBuffIndex][6 + byteNr*8] = (color & 0x02) ? WS2812_T1H : WS2812_T0H;
	dma_buff[dmaBuffIndex][7 + byteNr*8] = (color & 0x01) ? WS2812_T1H : WS2812_T0H;
}

static inline void WS2812_DMA_NullifyByte(uint8_t byteNr) {
	dma_buff[dmaBuffIndex][0 + byteNr*8] = 0;
	dma_buff[dmaBuffIndex][1 + byteNr*8] = 0;
	dma_buff[dmaBuffIndex][2 + byteNr*8] = 0;
	dma_buff[dmaBuffIndex][3 + byteNr*8] = 0;
	dma_buff[dmaBuffIndex][4 + byteNr*8] = 0;
	dma_buff[dmaBuffIndex][5 + byteNr*8] = 0;
	dma_buff[dmaBuffIndex][6 + byteNr*8] = 0;
	dma_buff[dmaBuffIndex][7 + byteNr*8] = 0;
}


static void WS2812_DMA_PrepareNext(void) {

	if (dmaBuffIndex >= WS2812_DMA_BUFFERS) dmaBuffIndex = 0;

	WS2812_DMA_PrepareByte(0, led_data[currentLed*3 + 0]);
	WS2812_DMA_PrepareByte(1, led_data[currentLed*3 + 1]);
	WS2812_DMA_PrepareByte(2, led_data[currentLed*3 + 2]);

	currentLed++;
	dmaBuffIndex++;
}

static void WS2812_DMA_NullufyNext(void) {

	if (dmaBuffIndex >= 2) dmaBuffIndex = 0;

	WS2812_DMA_NullifyByte(0);
	WS2812_DMA_NullifyByte(1);
	WS2812_DMA_NullifyByte(2);

	currentLed++;
	dmaBuffIndex++;
}



uint16_t * WS2812_DMA_GetBuffer(void) {
	return (uint16_t *)dma_buff;

}



void WS2812_TransferStart(void) {

	WS2812_DMA_InitBuffer();

	LL_DMA_DisableChannel(DMAx, LL_DMA_CHANNEL_5);
    LL_DMA_ClearFlag_TC5(DMAx);
    LL_DMA_ClearFlag_HT5(DMAx);

	LL_DMA_SetMemoryAddress (DMAx, LL_DMA_CHANNEL_5,  (uint32_t)dma_buff);

	LL_DMA_SetMode(DMAx, LL_DMA_CHANNEL_5, LL_DMA_MODE_CIRCULAR);
	LL_DMA_SetPeriphAddress(DMAx, LL_DMA_CHANNEL_5,  (uint32_t)(&TIMx->CCR1));
	LL_DMA_SetDataLength   (DMAx, LL_DMA_CHANNEL_5,  WS2812_DMA_TRANSFER_SIZE);
	LL_DMA_EnableIT_TC(DMAx, LL_DMA_CHANNEL_5);
	LL_DMA_EnableIT_HT(DMAx, LL_DMA_CHANNEL_5);
	LL_DMA_EnableChannel(DMAx, LL_DMA_CHANNEL_5);

	LL_TIM_EnableDMAReq_CC1(TIMx);
}

void WS2812_TransferStop (void) {
	LL_TIM_DisableDMAReq_CC1(TIMx);
	LL_DMA_DisableChannel(DMAx, LL_DMA_CHANNEL_5);
	LL_TIM_OC_SetCompareCH1(TIMx, 0);
}

