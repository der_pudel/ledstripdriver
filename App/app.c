/*
 * app.c
 *
 *  Created on: Dec 1, 2019
 *      Author: Igor
 */


#include <app.h>
#include <rtos_utils.h>
#include <WS2812.h>
#include "dma.h"
#include "tim.h"
#include "usb_device.h"
#include "gpio.h"
#include "rtos_utils.h"
#include "debug_utils.h"



void TaskMain(void *argument);
void TaskPeriodic(void *argument);

void FreeRTOS_Start(void) {


	vDebugInit();

	vDebugLog("Initializing FreeRTOS...");
	hwEvent = xEventGroupCreate();



	xTaskCreate( TaskMain,								/* The function that implements the task. */
				"Main", 								/* The text name assigned to the task */
				configMINIMAL_STACK_SIZE, 				/* The size of the stack to allocate to the task. */
				NULL,									/* The parameter passed to the task. */
				MAIN_TASK_PRIO, 		                /* The priority assigned to the task. */
				NULL );									/* The task handle*/


	xTaskCreate( TaskPeriodic,
				"Periodic",
				configMINIMAL_STACK_SIZE,
				NULL,
				MAIN_PERI_PRIO,
				NULL );

	vDebugLog("Initialization done");
	vDebugLog("Free heap: %d/%d bytes", xPortGetFreeHeapSize(), configTOTAL_HEAP_SIZE );
	/* Start the tasks and timer running. */
	vTaskStartScheduler();

}

void TaskMain(void *argument)
{
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_TIM15_Init();


	WS2812_Init();
	WS2812_SetBrightness(100);


uint8_t state = 0;
	MX_USB_DEVICE_Init();

	EventBits_t events;
	for(;;) {

		events = xEventGroupWaitBits(hwEvent, EVENT_HW_BTN, pdTRUE, pdFALSE, portMAX_DELAY  );

		if (events & EVENT_HW_BTN) {
			switch (state) {
				default:
					state = 0;
				case 0:
					WS2812_SetColorAll(0x01, 0x00, 0x00); break;
				case 1:
					WS2812_SetColorAll(0x00, 0x01, 0x00); break;
				case 2:
					WS2812_SetColorAll(0x00, 0x00, 0x01); break;

			}
			state++;

			WS2812_TransferStart();
			//vTaskDelay(pdMS_TO_TICKS(1000));

		}

	}
}

#define KEY_DEBOUNCE (5)

void TaskPeriodic(void *argument) {
	UNUSED(argument);

	uint8_t btnPrevState = 0xFF;
	uint8_t btnDebounce = 0xFF;

	TickType_t xLastWakeTime;
	const TickType_t xPeriod = pdMS_TO_TICKS(10);

	xLastWakeTime = xTaskGetTickCount();

	for (;;) {

		vTaskDelayUntil( &xLastWakeTime, xPeriod );


		if (btnPrevState != ((LL_GPIO_ReadInputPort(BUTTON_EXTI10_GPIO_Port) & BUTTON_EXTI10_Pin ) != 0) ) {
			btnDebounce = KEY_DEBOUNCE;
			btnPrevState = !btnPrevState;
		} else {
			if (btnDebounce && !--btnDebounce) {
				if (btnPrevState == 0) { // on press
					xEventGroupSetBits(hwEvent, EVENT_HW_BTN);
				} else { // on release

				}

			}
		}
	}

}

void vApplicationMallocFailedHook( void )
{
	/* vApplicationMallocFailedHook() will only be called if
	configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
	function that will get called if a call to pvPortMalloc() fails.
	pvPortMalloc() is called internally by the kernel whenever a task, queue,
	timer or semaphore is created.  It is also called by various parts of the
	demo application.  If heap_1.c or heap_2.c are used, then the size of the
	heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
	FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
	to query the size of free heap space that remains (although it does not
	provide information on how the remaining heap might be fragmented). */
	vDebugLog("malloc failed");
	taskDISABLE_INTERRUPTS();
	for( ;; );

}

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
	( void ) pcTaskName;
	( void ) pxTask;

	/* Run time stack overflow checking is performed if
	configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
	function is called if a stack overflow is detected. */
	vDebugLog("Stack overflow in task \"%s\"", pcTaskName);
	taskDISABLE_INTERRUPTS();
	for( ;; );
}
