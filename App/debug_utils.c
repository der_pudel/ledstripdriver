
#include "debug_utils.h"
#include "rtos_utils.h"
#include <stdio.h>

#if (ENABLE_DEBUG_PRINT)
#	include <stdarg.h>

extern void initialise_monitor_handles(void);

#endif

void vDebugInit(void) {

#if (ENABLE_DEBUG_PRINT)
	initialise_monitor_handles();
#endif

}

void vDebugLog(const char *format, ...) {

#if (ENABLE_DEBUG_PRINT)

	printf("> %010ld: ", xTaskGetTickCount()); // timestamp every message by RTOS tick count

    va_list args;
    va_start(args, format);
    vprintf(format, args);
    va_end(args);

    printf("\n");  // append line terminator
#endif

}


